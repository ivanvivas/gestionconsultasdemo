export class PacienteDto {
    idPaciente: number;
    nombre: string;
    edad: number;
    nhistoriaClinica: number;
    idHospital: number;
    tieneDieta: boolean;
    relacionPesEst: number;
    fumador: boolean;
    aniosFumador: number;
    prioridad: number;
    riesgo: number;
    fechaLlegada: Date;
    fueAtendido: boolean;
    idConsulta: number;

    constructor() {
      this.aniosFumador = null;
      this.edad = null;
      this.fechaLlegada = null;
      this.fueAtendido = false;
      this.fumador = null;
      this.nhistoriaClinica = null;
      this.idConsulta = null;
      this.idHospital = null;
      this.idPaciente = null;
      this.nombre = '';
      this.prioridad = null;
      this.relacionPesEst = null;
      this.riesgo = null;
      this.tieneDieta = null;
    }

    public setAniosFumador(anios: number): void {
      this.aniosFumador = anios;
    }

    public setEdad(edad: number): void {
      this.edad = edad;
    }

    public setfumador(fuma: boolean): void {
      this.fumador = fuma;
    }

    public setHistoriaClinica(historia: number): void {
      this.nhistoriaClinica = historia;
    }

    public setIdHospital(id: number): void {
      this.idHospital = id;
    }

    public setNombre(nombre: string): void {
      this.nombre = nombre;
    }

    public setPesoEstatura(relacion: number): void {
      this.relacionPesEst = relacion;
    }

    public setTieneDieta(dieta: boolean): void {
      this.tieneDieta = dieta;
    }
  }