export class ConsultaDto {
    idConsulta: number;
    cantPacientes: number;
    nombreEspecialista: string;
    tipoConsulta: string;
    estado: string;
    idHospital: number;

    constructor() {
      this.idConsulta = null;
      this.cantPacientes = null;
      this.nombreEspecialista = '';
      this.tipoConsulta = '';
      this.estado = '';
      this.idHospital = null;
    }
  }