import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';

@Component({
  selector: 'app-consulta-atendidos',
  templateUrl: './consulta-atendidos.component.html',
  styleUrls: ['./consulta-atendidos.component.css']
})
export class ConsultaAtendidosComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];

  constructor(private service: AppServiceService) {
    this.columnas = [
      'idConsulta',
      'cantPacientes',
      'nombreEspecialista',
      'tipoConsulta',
      'estado',
      'idHospital'
    ];
  }

  ngOnInit() {
    this.obtenerConsultaConMasPacientesAtendidos();
  }

  obtenerConsultaConMasPacientesAtendidos(): void {
    this.service.obtenerConsultaConMasPacientesAtendidos().subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }
}
