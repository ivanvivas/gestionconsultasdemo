import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-atender-pacientes',
  templateUrl: './atender-pacientes.component.html',
  styleUrls: ['./atender-pacientes.component.css']
})
export class AtenderPacientesComponent implements OnInit {

  constructor(
    private service: AppServiceService, 
    private router: Router) { 
  }

  ngOnInit() {
    this.atenderPacientes();
  }

  atenderPacientes(): void {
    this.service.atenderPacientes().subscribe(
      data => {
        console.log('data: ' + data);
        this.router.navigate(['/pacientesEspera']);
      },
      error => {
        console.log('error: ' + error.statusText);
        this.router.navigate(['/pacientesEspera']);
      }
    );
  }
}
