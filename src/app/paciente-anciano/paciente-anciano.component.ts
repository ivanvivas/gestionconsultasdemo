import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';

@Component({
  selector: 'app-paciente-anciano',
  templateUrl: './paciente-anciano.component.html',
  styleUrls: ['./paciente-anciano.component.css']
})
export class PacienteAncianoComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];

  constructor(private service: AppServiceService) {
    this.columnas = [
      'idPaciente', 
      'nombre', 
      'edad', 
      'nhistoriaClinica', 
      'idHospital', 
      'tieneDieta',
      'relacionPesEst', 
      'fumador',
      'aniosFumador',
      'prioridad',
      'riesgo',
      'fechaLlegada',
      'fueAtendido',
      'idConsulta'
    ];
  }

  ngOnInit() {
    this.obtenerPacienteMasAncianoEnEspera();
  }

  obtenerPacienteMasAncianoEnEspera(): void {
    this.service.obtenerPacenteMasAncianoEnEspera().subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }
}
