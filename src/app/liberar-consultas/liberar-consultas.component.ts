import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liberar-consultas',
  templateUrl: './liberar-consultas.component.html',
  styleUrls: ['./liberar-consultas.component.css']
})
export class LiberarConsultasComponent implements OnInit {

  constructor(
    private service: AppServiceService, 
    private router: Router) {
  }

  ngOnInit() {
    this.liberarConsultas();
  }

  liberarConsultas(): void {
    this.service.liberarConsultas().subscribe(
      data => {
        console.log('data: ' + data);
        this.router.navigate(['/pacientesEspera']);
      },
      error => {
        console.log('error: ' + error.statusText);
        this.router.navigate(['/pacientesEspera']);
      }
    );
    
  }
}
