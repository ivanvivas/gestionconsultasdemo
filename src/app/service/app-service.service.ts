import { Injectable } from '@angular/core';
import { Observable, Subject, asapScheduler, pipe, of, from, interval, merge, fromEvent } from 'rxjs';
import { map, filter, scan, catchError } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { PacienteDto } from '../model/PacienteDto';
import { ConsultaDto } from '../model/ConsultaDto';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AppServiceService {
  private numeroHistoriaClinica: number;
  private ENDPOINT_PACIENTE_RIESGOSO: string;
  private ENDPOINT_PACIENTE_FUMADOR: string;
  private ENDPOINT_PACIENTE_ANCIANO: string;
  private ENDPOINT_PACIENTE_ESPERA: string;
  private ENDPOINT_PACIENTE_INGRESO: string;
  private ENDPOINT_CONSULTA_MAS_PACIENTES: string;
  private ENDPOINT_ATENDER_PACIENTES: string;
  private ENDPOINT_LIBERAR_CONSULTAS: string;

  constructor(private http: HttpClient) {
    this.numeroHistoriaClinica = null;
    this.ENDPOINT_PACIENTE_RIESGOSO = 'http://localhost:8080/GestionConsultas/listarPacientesMayorRiesgo';
    this.ENDPOINT_PACIENTE_FUMADOR = 'http://localhost:8080/GestionConsultas/listarPacientesFumadoresUrgentes';
    this.ENDPOINT_PACIENTE_ANCIANO = 'http://localhost:8080/GestionConsultas/pacienteMasAncianoEnEspera';
    this.ENDPOINT_PACIENTE_ESPERA = 'http://localhost:8080/GestionConsultas/listarPacientesEnEspera';
    this.ENDPOINT_PACIENTE_INGRESO = 'http://localhost:8080/GestionConsultas/ingresarPaciente';
    this.ENDPOINT_CONSULTA_MAS_PACIENTES = 'http://localhost:8080/GestionConsultas/consultaMasPacientesAtendidos';
    this.ENDPOINT_ATENDER_PACIENTES = 'http://localhost:8080/GestionConsultas/atenderPacientes';
    this.ENDPOINT_LIBERAR_CONSULTAS = 'http://localhost:8080/GestionConsultas/liberarConsultas';
  }

  /**
   * Servicio para obtener desde el microservicio lista de pacientes mas riesgosos
   * que el indicado por No. de historia clinica pasado por parametro
   *
   * @param historiaClinica
   */
  obtenerPacientesMasRiesgosos(historiaClinica?: number): Observable<PacienteDto[]> {
    const requestObj = {
      historiaClinica: historiaClinica
    };
    return this.http.post<PacienteDto[]>(
      this.ENDPOINT_PACIENTE_RIESGOSO, 
      requestObj,
      httpOptions
    );  
  }

  /**
   * Servicio para ingresar paciente al sistema
   *
   * @param paciente
   */
  ingresarPaciente(paciente?: PacienteDto): Observable<void> {
    return this.http.post<void>(
      this.ENDPOINT_PACIENTE_INGRESO, 
      paciente,
      httpOptions
    );  
  }

  /**
   * Servicio para obtener desde el microservicio lista de 
   * pacientes fumadores urgentes
   */
  obtenerPacentesFumadoresUrgentes(): Observable<PacienteDto[]> {
    return this.http.get<PacienteDto[]>(
      this.ENDPOINT_PACIENTE_FUMADOR, 
      httpOptions
    );  
  }

  /**
   * Servicio para obtener desde el microservicio el 
   * paciente mas anciano en espera
   */
  obtenerPacenteMasAncianoEnEspera(): Observable<PacienteDto[]> {
    return this.http.get<PacienteDto[]>(
      this.ENDPOINT_PACIENTE_ANCIANO,
      httpOptions
    );  
  }

  /**
   * Servicio para obtener desde el microservicio la
   * consulta con mas pacientes atendidos
   */
  obtenerConsultaConMasPacientesAtendidos(): Observable<ConsultaDto[]> {
    return this.http.get<ConsultaDto[]>(
      this.ENDPOINT_CONSULTA_MAS_PACIENTES,
      httpOptions
    );  
  }

  /**
   * Servicio para obtener desde el microservicio los
   * pacientes en espera
   */
  obtenerPacientesEnEspera(): Observable<PacienteDto[]> {
    return this.http.get<PacienteDto[]>(
      this.ENDPOINT_PACIENTE_ESPERA,
      httpOptions
    );  
  }

  /**
   * Servicio para gestionar desde el microservicio la atencion de pacientes
   */
  atenderPacientes(): Observable<void> {
    return this.http.post<void>(
      this.ENDPOINT_ATENDER_PACIENTES,
      httpOptions
    );  
  }

  /**
   * Servicio para gestionar desde el microservicio la liberacion de consultas
   */
  liberarConsultas(): Observable<void> {
    return this.http.post<void>(
      this.ENDPOINT_LIBERAR_CONSULTAS,
      httpOptions
    );  
  }

  public getNumeroHistoriaClinica(): number {
    return this.numeroHistoriaClinica;
  }

  public setNumeroHistoriaClinica(historia: number): void {
    this.numeroHistoriaClinica = historia;
  }
}
