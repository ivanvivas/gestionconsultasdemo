import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title: string;
  mostrarAtenderPacientes: boolean;
  mostrarLiberarConsultas: boolean;
  mostrarPacientesRiesgo: boolean;
  mostrarPacientesFuma: boolean;
  mostrarConsultaMasPacientes: boolean;
  mostrarPacienteMasAnciano: boolean;

  constructor() {
    this.title = 'fonasaDemo';
    this.mostrarAtenderPacientes = false;
    this.mostrarLiberarConsultas = false;
    this.mostrarPacientesRiesgo = true;
    this.mostrarPacientesFuma = false;
    this.mostrarConsultaMasPacientes = false;
    this.mostrarPacienteMasAnciano = false;
  }

  ngOnInit() {
  }
}
