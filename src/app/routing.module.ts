import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AtenderPacientesComponent } from './atender-pacientes/atender-pacientes.component';
import { LiberarConsultasComponent } from './liberar-consultas/liberar-consultas.component';
import { PacientesMayorRiesgoComponent } from './pacientes-mayor-riesgo/pacientes-mayor-riesgo.component';
import { PacientesFumadoresComponent } from './pacientes-fumadores/pacientes-fumadores.component';
import { ConsultaAtendidosComponent } from './consulta-atendidos/consulta-atendidos.component';
import { PacienteAncianoComponent } from './paciente-anciano/paciente-anciano.component';
import { PacientesEsperaComponent } from './pacientes-espera/pacientes-espera.component';
import { IngresarUsuarioComponent } from './ingresar-usuario/ingresar-usuario.component';
import { PacientesSeleccionComponent } from './pacientes-seleccion/pacientes-seleccion.component';

const routes: Routes = [
  { path: 'atenderPacientes', component: AtenderPacientesComponent },
  { path: 'liberarConsultas', component: LiberarConsultasComponent },
  { path: 'pacientesMayorRiesgo', component: PacientesMayorRiesgoComponent },
  { path: 'pacientesFumadores', component: PacientesFumadoresComponent },
  { path: 'consultaAtendidos', component: ConsultaAtendidosComponent },
  { path: 'pacienteAnciano', component: PacienteAncianoComponent },
  { path: 'pacientesEspera', component: PacientesEsperaComponent },
  { path: 'ingresarUsuario', component: IngresarUsuarioComponent },
  { path: 'pacientesSeleccion', component: PacientesSeleccionComponent },
  { path: '', component: PacientesEsperaComponent }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class RoutingModule { }
