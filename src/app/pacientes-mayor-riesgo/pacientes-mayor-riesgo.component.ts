import { Component, OnInit } from '@angular/core';
import { PacienteDto } from '../model/PacienteDto';
import { AppServiceService } from '../service/app-service.service';

@Component({
  selector: 'app-pacientes-mayor-riesgo',
  templateUrl: './pacientes-mayor-riesgo.component.html',
  styleUrls: ['./pacientes-mayor-riesgo.component.css']
})
export class PacientesMayorRiesgoComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];
  private numeroHistoriaClinica: number;
  
  constructor(private service: AppServiceService) {
    this.columnas = [
      'idPaciente', 
      'nombre', 
      'edad', 
      'nhistoriaClinica', 
      'idHospital', 
      'tieneDieta',
      'relacionPesEst', 
      'fumador',
      'aniosFumador',
      'prioridad',
      'riesgo',
      'fechaLlegada',
      'fueAtendido',
      'idConsulta'
    ];
    this.numeroHistoriaClinica = this.service.getNumeroHistoriaClinica();
  }

  ngOnInit() {
    this.obtenerPacientesMasRiesgosos();
  }

  obtenerPacientesMasRiesgosos(): void {
    this.service.obtenerPacientesMasRiesgosos(this.numeroHistoriaClinica).subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }
}
