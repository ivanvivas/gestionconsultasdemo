import { Component, OnInit } from '@angular/core';
import { PacienteDto } from '../model/PacienteDto';
import { AppServiceService } from '../service/app-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ingresar-usuario',
  templateUrl: './ingresar-usuario.component.html',
  styleUrls: ['./ingresar-usuario.component.css']
})
export class IngresarUsuarioComponent implements OnInit {
  private nombre: string;
  private edad: number;
  private historiaClinica: number;
  private idHospital: number;
  private pesoEstatura: number;
  private dieta: boolean;
  private fuma: boolean;
  private aniosFumando: number;
  private datosPaciente: PacienteDto;

  constructor(private service: AppServiceService, private router: Router) {
    this.nombre = '';
    this.edad = null;
    this.historiaClinica = null;
    this.idHospital = null;
    this.pesoEstatura = null;
    this.dieta = false;
    this.fuma = false;
    this.aniosFumando = null;
    this.datosPaciente = new PacienteDto();
  }

  ngOnInit() {
  }

  ingresarPaciente(): void {
    console.log('nombre: ' + this.nombre);
    console.log('edad: ' + this.edad);
    console.log('historiaClinica: ' + this.historiaClinica);
    console.log('idHospital: ' + this.idHospital);
    console.log('pesoEstatura: ' + this.pesoEstatura);
    console.log('dieta: ' + this.dieta);
    console.log('fuma: ' + this.fuma);
    console.log('aniosFumando: ' + this.aniosFumando);
    
    // Construir objeto del request
    this.datosPaciente.setNombre(this.nombre);
    this.datosPaciente.setEdad(this.edad);
    this.datosPaciente.setHistoriaClinica(this.historiaClinica);
    this.datosPaciente.setIdHospital(this.idHospital);
    this.datosPaciente.setPesoEstatura(this.pesoEstatura);
    this.datosPaciente.setTieneDieta(this.dieta);
    this.datosPaciente.setfumador(this.fuma);
    this.datosPaciente.setAniosFumador(this.aniosFumando);

    this.ingresarPacienteServicio();
  }

  ingresarPacienteServicio(): void {
    this.service.ingresarPaciente(this.datosPaciente).subscribe(
      data => {
        console.log('data: ' + data);
        this.router.navigate(['/pacientesEspera']);
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }
}
