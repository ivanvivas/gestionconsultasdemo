import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { MatButtonModule, 
  MatTableModule, 
  MatFormFieldModule, 
  MatInputModule, 
  MatCheckboxModule, 
  MatSelectModule
} from '@angular/material';
import { AtenderPacientesComponent } from './atender-pacientes/atender-pacientes.component';
import { LiberarConsultasComponent } from './liberar-consultas/liberar-consultas.component';
import { PacientesMayorRiesgoComponent } from './pacientes-mayor-riesgo/pacientes-mayor-riesgo.component';
import { PacientesFumadoresComponent } from './pacientes-fumadores/pacientes-fumadores.component';
import { ConsultaAtendidosComponent } from './consulta-atendidos/consulta-atendidos.component';
import { PacienteAncianoComponent } from './paciente-anciano/paciente-anciano.component';
import { RoutingModule } from './routing.module';
import { HttpClientModule } from '@angular/common/http';
import { PacientesEsperaComponent } from './pacientes-espera/pacientes-espera.component';
import { IngresarUsuarioComponent } from './ingresar-usuario/ingresar-usuario.component';
import { PacientesSeleccionComponent } from './pacientes-seleccion/pacientes-seleccion.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    SideNavComponent,
    AtenderPacientesComponent,
    LiberarConsultasComponent,
    PacientesMayorRiesgoComponent,
    PacientesFumadoresComponent,
    ConsultaAtendidosComponent,
    PacienteAncianoComponent,
    PacientesEsperaComponent,
    IngresarUsuarioComponent,
    PacientesSeleccionComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RoutingModule,
    MatButtonModule,
    MatTableModule,
    MatInputModule,
    MatFormFieldModule,
    MatCheckboxModule,
    FormsModule,
    MatSelectModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
