import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pacientes-seleccion',
  templateUrl: './pacientes-seleccion.component.html',
  styleUrls: ['./pacientes-seleccion.component.css']
})
export class PacientesSeleccionComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];

  constructor(
    private service: AppServiceService,
    private router: Router) {
    this.columnas = [
      'idPaciente', 
      'nombre', 
      'edad', 
      'nhistoriaClinica', 
      'idHospital', 
      'tieneDieta',
      'relacionPesEst', 
      'fumador',
      'aniosFumador',
      'prioridad',
      'riesgo',
      'fechaLlegada',
      'fueAtendido',
      'idConsulta'
    ];
  }

  ngOnInit() {
    this.obtenerPacientesEnEspera();
  }

  obtenerPacientesEnEspera(): void {
    this.service.obtenerPacientesEnEspera().subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }

  /** 
   * Funcion para extraer el numero de historia clinica a partir del event
   * y setearlo en variable del service
  */
  seleccionPaciente(event: MouseEvent): void {
    console.log('Click: ' + event.srcElement.firstChild.nodeValue);
    this.service.setNumeroHistoriaClinica(Number(event.srcElement.firstChild.nodeValue));
    this.router.navigate(['/pacientesMayorRiesgo']);
  }
}
