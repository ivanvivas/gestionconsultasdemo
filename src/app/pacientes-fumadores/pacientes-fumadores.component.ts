import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';

@Component({
  selector: 'app-pacientes-fumadores',
  templateUrl: './pacientes-fumadores.component.html',
  styleUrls: ['./pacientes-fumadores.component.css']
})
export class PacientesFumadoresComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];

  constructor(private service: AppServiceService) {
    this.columnas = [
      'idPaciente', 
      'nombre', 
      'edad', 
      'nhistoriaClinica', 
      'idHospital', 
      'tieneDieta',
      'relacionPesEst', 
      'fumador',
      'aniosFumador',
      'prioridad',
      'riesgo',
      'fechaLlegada',
      'fueAtendido',
      'idConsulta'
    ];
  }

  ngOnInit() {
    this.obtenerFumadoresUrgentes();
  }

  obtenerFumadoresUrgentes(): void {
    this.service.obtenerPacentesFumadoresUrgentes().subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }

}
