import { Component, OnInit } from '@angular/core';
import { AppServiceService } from '../service/app-service.service';

@Component({
  selector: 'app-pacientes-espera',
  templateUrl: './pacientes-espera.component.html',
  styleUrls: ['./pacientes-espera.component.css']
})
export class PacientesEsperaComponent implements OnInit {
  private columnas: string[];
  private dataSource: any[];
  private cantPacientes: number;

  constructor(private service: AppServiceService) {
    this.columnas = [
      'idPaciente', 
      'nombre', 
      'edad', 
      'nhistoriaClinica', 
      'idHospital', 
      'tieneDieta',
      'relacionPesEst', 
      'fumador',
      'aniosFumador',
      'prioridad',
      'riesgo',
      'fechaLlegada',
      'fueAtendido',
      'idConsulta'
    ];
    this.cantPacientes = null;
  }

  ngOnInit() {
    this.obtenerPacientesEnEspera();
  }

  obtenerPacientesEnEspera(): void {
    this.service.obtenerPacientesEnEspera().subscribe(
      data => {
        console.log('data: ' + data);
        this.dataSource = data;
        this.cantPacientes = this.calcularCantidadDePacientesEnEspera();
      },
      error => {
        console.log('error: ' + error.statusText);
      }
    );
  }

  calcularCantidadDePacientesEnEspera(): number {
    var cantidad = this.dataSource == null ? 0 : this.dataSource.length;
    
    return cantidad;
  }
}
